module gitlab.com/alienspaces/holyragingmages/common/config

go 1.13

require (
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.4.0

	gitlab.com/alienspaces/holyragingmages/common/type/configurer v1.0.0
)

replace gitlab.com/alienspaces/holyragingmages/common/type/configurer => ../../common/type/configurer
