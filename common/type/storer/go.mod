module gitlab.com/alienspaces/holyragingmages/common/type/storer

go 1.13

require (
	github.com/jmoiron/sqlx v1.2.0
	google.golang.org/appengine v1.6.5 // indirect
)
