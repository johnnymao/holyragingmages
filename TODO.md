# Holy Raging Mages

## TODO

### Now

* Template `model`
  * Standard functions
  * Integrate into runner handler functions
* Common model
  * Initialization
    * Repositories

### Next

* Script `test-service $1`
* Script `generate`
* Service to service communications
* Mage service
* Other services

## License

COPYRIGHT 2020 ALIENSPACES alienspaces@gmail.com
